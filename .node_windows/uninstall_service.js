var Service = require('node-windows').Service;
var base = require('./service.js');

console.log('Uninstalling "', base.serviceName, '" from "', base.scriptPath, '"');

// Create a new service object
var svc = new Service({
  name: base.serviceName,
  script: base.scriptPath
});

// Listen for the "uninstall" event so we know when it's done.
svc.on('uninstall',function(){
  console.log('Uninstall complete.');
  console.log('The service exists: ', svc.exists);
});

// Uninstall the service.
svc.uninstall();