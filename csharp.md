---
title: C#
description: Everything about it.
published: true
date: 2023-08-07T07:44:28.633Z
tags: 
editor: markdown
dateCreated: 2020-02-25T12:45:29.728Z
---

# Summary

[Glossary](glossary/)

## Design time

- [Architecture](architecture/)
- [Analysers](analysers/)
- [Async programming](async/)
- [Basics](basics/)
- [Caching](caching/)
- [Configuration](configuration/)
- [Concurrency](concurrency/)
- [Dependency Injection](dependencyinjection/)
- [Logging](logging/)
- [Patterns](patterns/)
- [Performance](performance/)
- [Source generators](sourcegenerators/)
- [Unit testing & Integration testing (End to end tests)](unittests/)

## Build time

- [MSBuild](msbuild/)
- [Native compilation](nativecompilation/)
- [ProjectReference & PackageReference](projectpackagereference/)

## Runtime

- [Debugging / Profiling](debugprofile/)
- [GC & Memory Managment](garbagecollector/)

# Frameworks

- [.NET 6](net6/)
- [.NET Runtime](dotnetruntime/)
- [Azure Functions](azurefunctions/)
- [ASP.NET Core](aspnetcore/)
- [Blazor / Razor](razor/)
- [Entity Framework Core](efcore/)
- [Universal Windows Plateform (UWP)](uwp/)
- [WinUI](winui/)
- [Windows Service & Workers](workers/)

# News sources

* [Microsoft Docs dotnet](https://docs.microsoft.com/en-us/dotnet/csharp/)
* [.NET Ketchup](https://dotnetketchup.com/)
* [Repository of .NET content creators](https://github.com/matthiasjost/dotnet-content-creators)

# Courses

* [Async expert](https://academy.dotnetos.org/courses/take/async-expert-en/)
[Course's notes](async/course/)

# Tools

* [SharpLab](https://sharplab.io/)
See the generated C#, IL, JIT code.

# Third party libraries

## Patterns

* ❌[Prism](https://github.com/PrismLibrary/Prism)
Helps implementing the MVVM pattern.
I would recommend using `CommunityToolkit.Mvvm`.

* [CommunityToolkit.Mvvm](https://github.com/CommunityToolkit/dotnet)
Very useful classes to implement the MVVM pattern, such as TwoWay view binds or messages.

* [MediatR](https://github.com/jbogard/MediatR)
Helps implementing the CQRS pattern.

## Validation

* [FluentValidation](https://github.com/FluentValidation/FluentValidation)
Adds a layer of lambda-driven validation on top of your objets.

## Unit tests

* [Bogus](https://github.com/bchavez/Bogus)
A fake data generator for unit testing.

* [NSubstitute](https://nsubstitute.github.io/help/getting-started/)
A mocking framework for unit testing.

* [Moq](https://github.com/moq/moq4)
A mocking framework for unit testing.

* [FluentAssertions](https://fluentassertions.com/)
An assertion framework, replacing xUnit default static Asserts by extension methods. Goes well with `NSubstitute`.

## Mapping

* [AutoMapper](https://github.com/AutoMapper/AutoMapper)
Acts as a "converter" between two objects via a lambda-driven configuration.

* [Mapster](https://github.com/MapsterMapper/Mapster)
A mapping framework.

## ORM

* [EntityFramework Core](https://github.com/dotnet/efcore)
ORM and database managment for all types of databases.

## Logging

* [Serilog](https://github.com/serilog/serilog)
Logging framework with managed logging destinations.

## Serialization

* [CsvHelper](https://github.com/JoshClose/CsvHelper)
Serializer / Deserializer for csv files.

* [System.Text.Json](https://docs.microsoft.com/en-us/dotnet/api/system.text.json?view=net-6.0)
Included in .NET 5+. More performant than NewtonSoft. Also uses SourceGenerators, making it the even more performant.

## Tools

* [GuardClauses](https://github.com/ardalis/GuardClauses)
Write shorter checks at the start of a method.
  > Useless in .NET 5+ : The framework includes methods such as [ArgumentNullException.ThrowIfNull](https://learn.microsoft.com/en-us/dotnet/api/system.argumentnullexception.throwifnull?view=net-6.0) 
  {.is-info}

* [perfolizer](https://github.com/AndreyAkinshin/perfolizer)
Performance analysis tool

* [BenchmarkDotNet](https://github.com/dotnet/BenchmarkDotNet)
Code performance and benchmarking tool.

* [Windows Community Toolkit](https://github.com/windows-toolkit/WindowsCommunityToolkit)
UWP or .NET toolkit containing multiple helpers

* [Polly](https://github.com/App-vNext/Polly)
Handle patterns such a Retry, circuit breaker, fallback (and so on) in a thread safe manner.