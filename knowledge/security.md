---
title: Security
description: 
published: 1
date: 2020-02-25T11:46:15.232Z
tags: 
editor: undefined
dateCreated: 2020-02-25T11:46:13.765Z
---

<!-- SUBTITLE: A document to understand how security works through the WebServices -->

# Prerequisites
* Some knowledge about Http & Http Headers.

# Sources
* [jwt.io](https://jwt.io/introduction/)

# Summary

# JwtToken
## What's that ?

The `Jwt` (Json Web Token)  is used to authenticate a service, and is passed through the Http Header `Authorization`, following the value `Bearer`.

```
// Key: Value
Authorization: Bearer <Jwt>
```

The `Jwt` is seperated by 3 parts : The header, the payload (body) and the signature, each is converted in **Base64** and separated by dots.

```
xxx.yyyy.zzzz
```

The header contains the algorithm used to make the signature:

```json
{
  "alg": "HS256",
  "typ": "JWT"
}
```

## How does it authenticate ?

Taking the example of a web service, a web service can check the signature of the `Jwt`, with a public key if the algorithm is **asymmetric**. 