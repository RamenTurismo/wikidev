---
title: Netcore
description: 
published: 1
date: 2020-02-25T11:46:10.552Z
tags: 
editor: undefined
dateCreated: 2020-02-25T11:46:09.110Z
---

# Using .NetCore 3 (Preview) w/ VS 2019
Download it [from here](https://dotnet.microsoft.com/download/dotnet-core/3.0).
Then activate it on Visual Studio 2019:
Go to Tools > Options > General > Environment and tick the checkbox.