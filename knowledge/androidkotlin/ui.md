---
title: UI
description: 
published: 1
date: 2020-02-25T11:46:31.920Z
tags: 
editor: undefined
dateCreated: 2020-02-25T11:46:30.313Z
---

<!-- SUBTITLE: This page is about everything that concerns the UI -->

# Warning: [is a boxed field but needs to be un-boxed to execute] with MutableLiveData{Int}
The solution is **NOT** to use the method `safeUnbox()`, you got to have that dev intuition.

The solution is to replace the `MutableLiveData{Int}` by `ObservableInt()` if your value is `Int`, same works for other types.

# SPAN_EXCLUSIVE_EXCLUSIVE spans cannot have a zero length

Caused by the external keyboard apparently.

In the `com.google.android.material.textfield.TextInputEditText` change:
```xml
android:inputType="text"
```
to
```xml
android:inputType="text|textNoSuggestions"
```
## Sources
https://stackoverflow.com/a/13696084/5794842

# Push up the elements when keyboard opens

The solution found here was to push up the whole activity, which renders well.
We can do that by adding some code on `OnCreate`:

```kotlin
this.window.setSoftInputMode(
		WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
)
```

## Sources
https://stackoverflow.com/a/28012530/5794842
# Put an element in the bottom of the activity

To do that, we can use `RelativeLayout`:

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:tools="http://schemas.android.com/tools"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        android:layout_width="match_parent"
        android:orientation="vertical"
        android:layout_height="match_parent">
```

And our element will look like:

```xml
    <Button
            android:layout_width="match_parent"
            android:layout_height="200sp"
            android:layout_alignParentBottom="true"
            android:background="@color/colorPrimary"
            android:textColor="@android:color/white"
            android:textSize="18sp"
            android:textStyle="bold"/>
```

## Sources
https://stackoverflow.com/a/54204289/5794842