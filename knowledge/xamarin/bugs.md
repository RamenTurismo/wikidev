---
title: Xamarin: Q&A Bugs
description: 
published: 1
date: 2020-02-25T11:47:07.148Z
tags: 
editor: undefined
dateCreated: 2020-02-25T11:47:05.746Z
---

# Clearing storage makes the app crash
Refer to [Couldn't connect to logcat](#couldnt-connect-to-logcat).

# Couldn't connect to logcat
**Devices:** Android

## Description

When this message appears:

```
Couldn't connect to logcat, GetProcessId returned: 0
```

 ## Solution

Go to the Android project, Under `Properties`, `Android Options`, and uncheck `Use Shared Runtime`.