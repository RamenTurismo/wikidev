---
title: Kotlin
description: 
published: 1
date: 2020-02-25T11:46:45.122Z
tags: 
editor: undefined
dateCreated: 2020-02-25T11:46:43.781Z
---

<!-- SUBTITLE: A quick summary of Kotlin -->

# About Android Frameworks
## Sources
* [kotlinlang.org](https://kotlinlang.org/docs/tutorials/android-frameworks.html)

# MVVM
## Sources

* [proandroiddev.com](https://proandroiddev.com/mvvm-with-kotlin-android-architecture-components-dagger-2-retrofit-and-rxandroid-1a4ebb38c699)
