---
title: Android with Kotlin
description: 
published: 1
date: 2020-02-25T11:45:56.390Z
tags: 
editor: undefined
dateCreated: 2020-02-25T11:45:54.821Z
---

<!-- SUBTITLE: Everything I learnt about Android with Kotlin -->

# Experience
* [Date](androidkotlin/date)
* [UI](androidkotlin/ui)
# How to start a project from A to Z
## Pre-requisites
* Android Studio, latest version if possible.
* Brain (Your own)

## Architectures

[android-architecture](https://github.com/googlesamples/android-architecture)

This repository contains multiple architectures of an App:

* MVP
* MVVM

### Which one to choose though ?

MVVM seems harder to implement at first, but it does takes less unit tests to create, meaning less maintenance.

So I'd go for MVVM and the learning curve of the DataBinding library.

### Implementing MVVM

I found a few sources:

* [proandroiddev.com/mvvm-with-kotlin-android-architecture-components-dagger-2-retrofit-and-rxandroid](https://proandroiddev.com/mvvm-with-kotlin-android-architecture-components-dagger-2-retrofit-and-rxandroid-1a4ebb38c699)

## Project conversion to AndroidX

I'll be using `AndroidX` type of project, to enable DataBinding and to stay up to date with every component that is updated.

To convert an existing project to `AndroidX`, on Android Studio, `Refractor` > `Migrate to AndroidX...`.

### Sources
[Documentation](http://kodein.org/Kodein-DI/)


# Understanding Kotlin language

## object

`object` is a thread-safe singleton (static).

## companion object

`companion object` is a way to say that everything inside this is static and directly accessible.

### Exemple

```kotlin
class DummyClass
	companion object {

	// Static method
	fun newInstance(param1: String, param2: String) =
		BlankFragment().apply {
			arguments = Bundle().apply {
					putString(ARG_PARAM1, param1)
					putString(ARG_PARAM2, param2)
			}
		}
	}
}

// Some other code

DummyClass.newInstance("test", "test")

```

## Sources
* [companion-object-in-kotlin](https://medium.com/@agrawalsuneet/companion-object-in-kotlin-5251e03d6423)
* [antonioleiva.com/objects-kotlin](https://antonioleiva.com/objects-kotlin/)

# Dependency injection
I wanted to delegate the fact of making instances to a container. I see many devs putting `companion object` with a factory method, but I don't think that is the proper way to do it as that means we'll also have to manage its lifetime.

## DI #1 : Kodein

`Kodein` is just a container that'll retrieve instances as we'd like. It uses scopes such a transcient (`factory`), `singleton`, etc.
It does what I want : getting an instance (by lazy), without having to manage anything.
*But*
Downside is, their is no viewmodel support, meaning `binding` an instance as `provider` will return a new instance everytime, and I can't find something that does exactly what I want.

Kodein also uses `androidx` libraries.

### Declaration of dependencies

Using `Application()` inheritance:

```kotlin
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton

class MyApp : Application(), KodeinAware {
    override val kodein by Kodein.lazy {
        /* bindings goes here */
    }
}
```

### Retrieval

From an activity that inherits the interface `KodeinAware` and overrides the `kodein` :

```
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.direct
import org.kodein.di.generic.instance

// ...

override val kodein by kodein()
```

### Sources
https://github.com/Kodein-Framework/Kodein-DI
https://kodein.org/Kodein-DI/

## DI #2 : Koin

This **has** the viewModel support, and it works well. Easy to put in place, with their module system (although kodein has it as well):

```kotlin
// In AppModules.kt for example, just put in like that in the file

val dataModule = module {
    single {
        MyDatabaseInstance(get(), get()) // Two dependencies
    }
}
```

And in `App.kt` where the class inherits from `android.app.Application`:

```kotlin
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        // Start Koin
        startKoin {
            androidContext(this@App)
            androidLogger()
						modules(listOf(dataModule))
        }
    }

}
```


### Sources
https://insert-koin.io/docs/2.0/getting-started/android/
[https://github.com/InsertKoinIO/getting-started-koin-android](https://github.com/InsertKoinIO/getting-started-koin-android/tree/master/app/src/main/kotlin/org/koin/sample)

# Supporting multiples cultures (strings.xml)
## Sources
* [developer.android.com](https://developer.android.com/training/basics/supporting-devices/languages)
# Room
`Room` is a dependency to manage entities and database.

[Room Persistence Library](https://developer.android.com/topic/libraries/architecture/room)

## Creating a Database service to communicate with the underlying database

Here's a simple example:

```kotlin
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

/**
 * Represents the Database used.
 */
@Database(
    entities =
    [
        MyEntity::class,
        OurEntity::class
    ],
    version = 1
)
@TypeConverters(DateTypeConverter::class)
abstract class AppDatabase : RoomDatabase()
```

The `@TypeConverters(DateTypeConverter::class)` is to convert a `String` to a `Date` object, the `String` being in *ISO 8601*.
This converter is custom ! See below [Manage Date in Kotlin](#manage-date-in-kotlin).

## Manage Date in Kotlin
Although it seems that `Room` does not support objects of Date. We can use `OffsetDateTime`, which looks the best object to use, because it supports timezones.

We have to tell `Room` how to convert it:

```kotlin
class DateTypeConverter {

    @TypeConverter
    fun toOffsetDateTime(value: String?): OffsetDateTime? {
        return value?.let {
            return formatter.parse(value, OffsetDateTime::from)
        }
    }

    @TypeConverter
    fun fromOffsetDateTime(date: OffsetDateTime?): String? {
        return date?.format(formatter)
    }

    companion object {
        private val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME
    }
}
```

*SQLite* supports `DateTime` in String format, as I saw on another project using SQLite in C#.

### Sources
[medium.com](https://medium.com/androiddevelopers/room-time-2b4cf9672b98)

# Creating a Drawer app with navigation

## Sources
https://developer.android.com/guide/navigation/navigation-ui#add_a_navigation_drawer
