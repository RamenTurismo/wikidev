---
title: Microsoft OpenId
description: 
published: 1
date: 2020-02-25T11:46:08.245Z
tags: 
editor: undefined
dateCreated: 2020-02-25T11:46:06.770Z
---

<!-- SUBTITLE: Explains how to login with a Microsoft account on a website. -->

# Get started
What we need here is two applications :
* A .Netcore 2.1 web app
* A .Netcore 2.1 web Api
* An Azure Active Directory

## Samples
[How to connect with the Azure AD with .NetCore 2.1](https://github.com/Azure-Samples/active-directory-dotnet-webapp-webapi-openidconnect)

# Configuration
## WebApp
### appconfig.json

> It is recommended to use the [app secrets](https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-2.1&tabs=windows)
