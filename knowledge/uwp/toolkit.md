---
title: Toolkit
description: 
published: 1
date: 2020-02-25T11:47:02.743Z
tags: 
editor: undefined
dateCreated: 2020-02-25T11:47:01.406Z
---

# DataGrid
## ToggleSwitch doesn't work

The ``ToggleSwitch`` component may not work at all in the ``DataGrid`` row.
To fix this, you have to find the ``DataGrid`` style and find the element that contains ``x:Name="RowPresenter"``.

Change the ``ManipulationMode`` to ``ManipulationMode="None"``.

```csharp
<localprimitives:DataGridRowsPresenter
		x:Name="RowsPresenter"
		Grid.Row="1"
		Grid.RowSpan="2"
		Grid.ColumnSpan="3"
		AutomationProperties.AccessibilityView="Raw"
		ManipulationMode="None" />
```

## Add a column programmatically

This **doesn't** seem possible:

```csharp
private void AddData()
{
  // From the component resources.
  bool a = this.DataGrid.Resources.TryGetValue("LivCellResourceDataTemplate", out object cellTemplate);
  bool b = this.DataGrid.Resources.TryGetValue("LivHeaderResourceStyle", out object headerStyle);

  int displayIndex = this.DataGrid.Columns
    .Where(x => x.Tag != null && (x.Tag as string) == "LivGenerated")
    .Select(x => x.DisplayIndex + 1)
    .LastOrDefault();

  if (displayIndex == default)
  {
    displayIndex = 3;
  }

  this.DataGrid.Columns.Add(new DataGridTemplateColumn()
  {
    Width = new DataGridLength(15, DataGridLengthUnitType.Star),
    DisplayIndex = displayIndex,
    HeaderStyle = headerStyle as Style,
    CellTemplate = cellTemplate as DataTemplate,
    Tag = "LivGenerated"
  });
}
```

It will add an empty column. It actually ignores the two resources retrieved at the start of the method.