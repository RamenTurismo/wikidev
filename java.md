---
title: Java
description: 
published: true
date: 2022-03-02T09:53:19.715Z
tags: 
editor: markdown
dateCreated: 2021-12-17T08:38:53.264Z
---

# Summary

- [Unit testing](/java/unittesting)

# Frameworks

- [Spring](spring/)
Dependency injection, test framework

- SpringBoot

# Libraries

- JPA
ORM

- [Flyway](https://flywaydb.org/documentation/)
DB Migration manager

- Lombok
Utility to generate code at design time.