---
title: Debug
description: 
published: true
date: 2021-11-03T13:34:18.769Z
tags: debug
editor: markdown
dateCreated: 2021-11-03T13:34:07.998Z
---

# Debug with VSCode

[This tutorial](https://code.visualstudio.com/docs/typescript/typescript-tutorial) explains it well.

To debug, launch this commande with `npm` :

```
npm install -g typescript
```

then

```
tsc <file.ts>
```

to create a `.js` file next to it. And then to launch it:

```
node <file.js>
```