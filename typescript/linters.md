---
title: Linters
description: Code check up tools
published: 1
date: 2021-02-19T10:53:43.903Z
tags: 
editor: markdown
dateCreated: 2021-02-19T10:53:43.903Z
---

# What's a linter ?

A plugin on your IDE or compiler to check for inconsistencies with a set of rules in the code that's been writen.

# TSLint

> Deprecated for ESLint
{.is-warning}

Some tools exists [to migrate the current `tslint.json` to a eslint configuration](https://code.visualstudio.com/api/advanced-topics/tslint-eslint-migration).

# ESLint

// TODO Commands
