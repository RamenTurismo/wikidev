---
title: Async Await (Promises, Observables)
description: How to use asynchronous operations
published: 1
date: 2021-02-19T10:36:21.245Z
tags: 
editor: markdown
dateCreated: 2021-02-19T10:36:21.245Z
---

# Sources

* [Using async-await feature in Angular](https://medium.com/@balramchavan/using-async-await-feature-in-angular-587dd56fdc77)

# Observable usages

> `subscribe()` is from `Observable` object. Once subscribed, `subscribe()` callback shall be executed **whenever there is a new data produced** by `Observer`. 
> 
> Whereas `promise`'s `then()` callback handler shall be executed at max **once**.
> {.is-info}
