---
title: Powershell
description: 
published: 1
date: 2020-11-16T16:24:35.405Z
tags: 
editor: undefined
dateCreated: 2020-11-16T16:22:04.154Z
---

# Windows Store Apps

## Get a package by name

```powershell
Get-AppxPackage -allusers -Name *myPackage*
```

`myPackage` could be replaced by the id of the app.

## Remove a package

```powershell
Remove-AppxPackage -Package "myPackage" -allusers
```

# Installing Powershell Core

> The .NET Core SDK is a prerequisite.
{.is-info}

```powershell
dotnet tool install --global powershell
```