---
title: General
description: General informations with no relation to a specific language
published: true
date: 2021-11-17T20:03:36.510Z
tags: 
editor: markdown
dateCreated: 2020-02-25T11:45:48.484Z
---

# Summary

## Languages

* [C#](/csharp)
* [Kotlin](/kotlin)
* [Javascript](/javascript)
* [Powershell](/powershell)
* [TypeScript](/typescript)

## Common

* [Concepts](concepts/)
Basics concepts applied independently from any language
* [Databases](databases/)
* [Glossary](glossary/)
Words about development things
* [Tools](tools/)
Tools used while programming
* [Web](web/)
General information about web technologies

## Network

- [Network topics](network/)

- [Apache Kafka](network/apachekafka)
Streaming data
- [Nginx](network/nginx)
Reverse proxy and html server