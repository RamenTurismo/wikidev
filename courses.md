---
title: Courses
description: Different courses that I followed online.
published: 1
date: 2021-04-15T07:33:35.267Z
tags: 
editor: markdown
dateCreated: 2020-09-25T07:34:28.103Z
---

# Async Expert [August 2020]

* [Official link](https://academy.dotnetos.org/courses/take/async-expert-en/)
* [Notes](/csharp/async/course)

# Kotlin Architecture components

* [Official link](https://www.udemy.com/course/android-kotlin-developpez-des-apps-next-gen/learn/lecture/11602174?start=0#overview)

# Microsoft Ignite App Maker Challenge [September 2020]

* [Official link](https://docs.microsoft.com/en-gb/users/cloudskillschallenge/collections/moqrtxx6x324?WT.mc_id=cloudskillschallenge_A3087E6F-E8A4-4365-B132-F8BE2E5A725D)