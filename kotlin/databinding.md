---
title: DataBinding
description: Implementing databinding with android
published: 1
date: 2021-04-21T09:32:07.090Z
tags: 
editor: markdown
dateCreated: 2021-04-21T08:06:44.673Z
---

# Requisites

# XML Declaration

A typical databinding file would look like this :

```xml
<layout xmlns:android="http://schemas.android.com/apk/res/android">
   <data>
       <variable name="viewModel" type="com.example.MyViewModel"/>
   </data>
  
  <!-- Components -->
  <LinearLayout />
</layout>
```

# XML Syntax

* [Data Binding Expressions](https://developer.android.com/topic/libraries/data-binding/expressions)

## Call a method

```xml
android:onClick="@{() -> viewModel.submit()}"
```