---
title: ASP.NET Core
description: The only true framework.
published: 1
date: 2020-10-30T09:08:37.442Z
tags: 
editor: undefined
dateCreated: 2020-03-03T08:29:52.827Z
---

# Launch a background operation

> Research in progress
{.is-warning}

`Task.Run` shouldn't be used, as if the services are scoped, they will be disposed at the end of the request.

Sources
https://stackoverflow.com/a/45015004

# Options

Option pattern is something to use to configure a service.

Note that it is often wrapped in an extension method as so:
```csharp
services.AddMyService(options => { ... });
```
Where the body of the extension is
```csharp
public static class MyServiceExtensions
{
	public static IServiceCollection AddMyService(this IServiceCollection services, Action<MyOptions> configure)
  {
  	services.Configure<MyOptions>(configure);
		services.TryAddScoped<IMyService, MyService>();

    return services;
  }
}
```

## Basics

It is done via the method `ConfigureServices` in `Startup.cs`:

```csharp
services.Configure<MyOptions>(configure => Configuration.Bind("MyConfig:Options", configure));
```

Where `Configuration` can be obtain via injecting it in the startup class constructor.

This allow any service to inject `IOptions<MyOptions>`. But there is also other interfaces that can be injected, such as `IOptionsFactory<MyOptions>` for named options.

## Options with injected services

To add options that depends on services, even `IConfiguration`:
```csharp
services.AddOptions<MyOptions>()
	.Configure<MyOptions, IConfiguration, IMyService>((options, conf, service) => ...);
```

## Sources

* https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/options?view=aspnetcore-3.1

# JwtToken

## Sources

* https://www.c-sharpcorner.com/article/asp-net-core-web-api-creating-and-validating-jwt-json-web-token/
* https://www.c-sharpcorner.com/article/implement-jwt-in-asp-net-core-3-1/

# Blazor

Use blazor when you want to build a Single Page Application.
It takes the principle of building an app via components.

`Blazor` uses the `Razor` syntax.

## Samples

* http://www.appvnext.com/blog/2020/3/7/blazor-shopping-cart-sample-using-local-storage-to-persist-state

## Sources

* https://www.telerik.com/blogs/difference-between-blazor-vs-razor