---
title: WinUI
description: Explanation about the Win UI framework in .NET
published: true
date: 2023-08-07T07:42:48.086Z
tags: 
editor: markdown
dateCreated: 2021-05-12T13:57:38.284Z
---

# Summary

WinUI is a framework that offers components and core functionnalities for the UI.

WinUI 2 is tightly coupled in UWP apps but the arrival of WinUI 3 offers a decoupled framework that allows to use .NET 5 and UWP APIs at the same time.

# Compile a portable executable

PubXml

```xml
<?xml version="1.0" encoding="utf-8"?>
<!--
https://go.microsoft.com/fwlink/?LinkID=208121.
-->
<Project ToolsVersion="4.0" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <PropertyGroup>
    <PublishProtocol>FileSystem</PublishProtocol>
    <Platform>x64</Platform>
    <PublishDir>bin\win10-x64\publish\</PublishDir>
    <SelfContained>false</SelfContained>
    <PublishReadyToRun Condition="'$(Configuration)' == 'Debug'">False</PublishReadyToRun>
    <Configuration>Release</Configuration>
    <TargetFramework>net6.0-windows10.0.19041.0</TargetFramework>
    <!-- 
    See https://github.com/microsoft/CsWinRT/issues/373
    <PublishTrimmed>True</PublishTrimmed>
    -->
  </PropertyGroup>
</Project>
```

It could fail at runtime with a missing library. You could try [by disabling](https://github.com/microsoft/WindowsAppSDK/issues/1883#issuecomment-990217706) the `WinUISDKReferences` in the csproj.
