---
title: .NET 6
description: Articles about .NET 6
published: true
date: 2022-07-12T10:02:09.716Z
tags: 
editor: markdown
dateCreated: 2021-11-03T13:46:11.949Z
---

# [Entity Framework Community Standup - Make History and Explore the Cosmos, an EF 6 Retrospective](https://www.youtube.com/watch?v=cx6IUURncgk)

## EFCore : CosmosDb

- Configuration now supports automatic `Owns` relationships.
- Does not support subqueries where, for example, we want to search in an array of `int` that is contained in a complex type.

## EFCore : Temporary Tables

> Only available with SQL.
{.is-warning}

This feature can store the entity to an history table.

# [Series: Exploring .NET 6](https://andrewlock.net/series/exploring-dotnet-6/)
- Part 1 - Looking inside ConfigurationManager in .NET 6
- Part 2 - Comparing WebApplicationBuilder to the Generic Host
- Part 3 - Exploring the code behind WebApplicationBuilder
- Part 4 - Building a middleware pipeline with WebApplication
- Part 5 - Supporting EF Core migrations with WebApplicationBuilder
- Part 6 - Supporting integration tests with WebApplicationFactory in .NET 6
- Part 7 - Analyzers for ASP.NET Core in .NET 6
- Part 8 - Improving logging performance with source generators