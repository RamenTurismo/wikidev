---
title: .NET Native tool chain
description: 
published: 1
date: 2020-10-08T14:14:34.110Z
tags: 
editor: undefined
dateCreated: 2020-09-29T15:21:58.885Z
---

# TL;DR

## Pros

- Optimizes code.

## Cons

- Slows execution times for methods that uses reflection
- Potentially introduces new behavior, that could crash the app

# Runtime directives: Default.rd.xml

[Runtime Directives (rd.xml) Configuration File Reference](https://docs.microsoft.com/en-us/dotnet/framework/net-native/runtime-directives-rd-xml-configuration-file-reference)
