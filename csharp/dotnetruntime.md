---
title: Dotnet Runtime
description: Talking about the command line dotnet. 
published: 1
date: 2020-11-18T12:18:37.204Z
tags: 
editor: undefined
dateCreated: 2020-10-20T12:00:48.177Z
---

# Powershell Core

[See here](/powershell#installing-powershell-core)

# NuGet

## Clear all cache

```
dotnet nuget locals all --clear
```   
  
# Outdated : Checking outdated dependencies

Checking for outdated dependencies in projects, and shows them with a coloured syntax.

## Installation

```powershell
dotnet tool install --global dotnet-outdated-tool
```

## Scan

```powershell
dotnet outdated
```

# EF

## Install as a global tool

```
dotnet tool install --global dotnet-ef
```

## Add a migration

```
dotnet ef migrations add <Name>
```

## Remove a migration (Last one)

```
dotnet ef migrations remove
```

## Upgrade database

```
dotnet ef database update
```

## Downgrade database

```
dotnet ef database update <NameOfPreviousMigration>
dotnet ef migrations remove
```