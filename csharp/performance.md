---
title: Performance
description: Describes everything that can be optimized in C#.
published: true
date: 2023-01-05T08:29:32.828Z
tags: 
editor: markdown
dateCreated: 2020-10-19T11:39:41.679Z
---

# Sources

* [Performance best practices in C# by Kevin Gosse](https://medium.com/@kevingosse/performance-best-practices-in-c-b85a47bdd93a)

# Passing parameters as reference

To optimize memory allocation, we could use the keywords `in` or `ref` for parameters in a method.

```csharp
public void Do(in ReadOnlySpan<char> myString, in ReadOnlyStruct myNumber)
{
  // Stuff
}
```

Another keyword that passes as a reference is `out`.

> Using the keyword `in` is useless and could lead to worse performance if used with primitve types.
{.is-info}

> C# doesn't allow reference passed arguments in **Expression** types or **async** methods. 
{.is-info}

> Be aware of defensive copies.
> [Avoiding struct and readonly reference performance pitfalls with ErrorProne.NET](https://devblogs.microsoft.com/premier-developer/avoiding-struct-and-readonly-reference-performance-pitfalls-with-errorprone-net/)
{.is-warning}

# Span[T] and ReadOnlySpan[T]

`Span<T>` is a value type.

> Because it is a **stack only** value type (ref struct), this could exists longer on the heap and not get garbage collected. 
> Cannot be used as variable in async methods.
> Can't be captured in lambda expressions.
> Can't be boxed.
{.is-warning}

`Span<T>` and `ReadOnlySpan<T>` are both "views" to native pointers, avoiding allocation.
Before their venue into the C# world, we had to use unsafe code to use pointers that points to the memory bit we wanted to change.

Use the keyword `in` or `ref` to pass one of these as a reference, once again avoiding allocation.

## Example

```csharp
int[] array = new { 1, 2, 3, 4, 5 };

// Linq has overhead
int[] arraySplit = array.Take(2).Skip(1).ToArray();

// Gives the same result without the allocation and the overhead (execution time).
Span<int> arraySpan = array.AsSpan().Slice(1, 2);
```

# Memory[T], ReadOnlyMemory[T]

Can be on the heap and put in a class, thus, avoiding the need to reconceptualize a class to a `ref struct` of sorts.
  
# Avoid boxing

An example of boxing:

```csharp
// C# 10 Top Level Statements

// Creates the effect of boxing my adding an object to the Heap, allocating memory.
Test(new MyStruct());

public readonly struct MyStruct : IMyInterface
{
  // From IMyInterface
  public int Number { get; }
}

void Test(IMyInterface obj)
{
  // Code
}
```

Taking the piece of code above, we can see that the method `Test` uses boxing, thus allocating more memory, generating more code and defeating the principle of using the `struct`.

To avoid these kind of problems, you need to use **generics**.
This gives us:
```csharp
public void Test<T>(T obj)
  where T : IMyInterface
{
  // Code
}
```

# Measuring time

## Approximative measurements

The most common and optimized way would be using `Stopwatch` in `System.Diagnostics`.

It is not necessary to call `Stopwatch.Stop()` unless useful for the use case. `Stopwatch` doesn't use any I/O or CPU and `.Stop` doesn't free up resources. 

Here is the [source code of an implementation in .NET Core](https://github.com/dotnet/corefx/blob/b8b81a66738bb10ef0790023598396861d92b2c4/src/Common/src/CoreLib/System/Diagnostics/Stopwatch.cs).

This technique is not really accurate, as it adds code for the compiler to interpret.

## Precise measurements

Using the package [BenchmarkDotNet](https://github.com/dotnet/BenchmarkDotNet) and creating micro-benchmarks for the methods to be tested.