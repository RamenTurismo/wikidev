---
title: Analysers
description: 
published: 1
date: 2021-05-10T13:29:04.248Z
tags: analyser, c#
editor: markdown
dateCreated: 2020-03-05T08:11:06.559Z
---

# Create an analyser

## Source

* https://devblogs.microsoft.com/dotnet/how-to-write-a-roslyn-analyzer/

# Base analyzers

* [Microsoft.CodeAnalysis.NetAnalyzers](https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/overview)