---
title: Unit testing
description: Unit testing & Integration testing with .NET
published: true
date: 2022-03-02T09:50:15.169Z
tags: 
editor: markdown
dateCreated: 2020-02-28T10:19:57.481Z
---

# Unit testing

## Mocking IConfiguration

The way to mock the IConfiguration interface from `Microsoft.Extensions.Configuration` is mocking the `GetSection` method.
Extensions are using this method.

It is also much simplier to just use `ConfigurationBuilder` and do `AddInMemoryCollection` :

```csharp
IConfigurationRoot configuration = new ConfigurationBuilder()
  .AddInMemoryCollection(new Dictionary<string, string>
  {
    { "Logging:LogLevel:Default", "Information" }
  })
  .Build();
```

## Using FluentAssertions

FluentAssertions is an extension framework wich replaces xUnit's `Assert.`.

Advantages of using it is extensability : We can create our own assertions classes and make them as extensions, wich makes them reusable everywhere.

Exceptions are also way clearer when it comes to compare it with xUnit.

## Using NSubsitute

`NSubsitute` is an extension framework, that allows to mock instances without the burden of using another wrapping class on top of it (Such as `Mock<MyService>()`).

# Test Internal methods

Add the lines [described here](/csharp/projectpackagereference#internal-method-reference-from-another-project).

# Integration tests

"Integration tests" is just a fancy set of words to indicate that the tests are performed on an environment simulating the webservice, with its services not mocked but using the real implementations.

[Here's an example of basic implementation](https://github.com/jbogard/ContosoUniversityDotNetCore-Pages/tree/master/ContosoUniversity.IntegrationTests)

It uses the `WebApplicationFactory<T>` to build a `Host` or `WebHost` that takes into account the startup class or any class that is specified in the overrided create method.