---
title: Miscellaneous
description: https://academy.dotnetos.org/courses/take/async-expert-en/lessons/13606707-ivaluetasksource-with-interlocked-compareexchange
published: true
date: 2022-02-15T14:37:39.955Z
tags: 
editor: markdown
dateCreated: 2020-06-19T08:48:39.612Z
---

# IValueTaskSource with Interlocked.CompareExchange

It is a Task-like concept around `ValueTask`.
It is awaitable because it uses the same semantics of an Awaiter.

[Implementing custom IValueTaskSource – async without allocations](https://tooslowexception.com/implementing-custom-ivaluetasksource-async-without-allocations)

# ManualResetValueTaskSourceCore

- ManualReset : Does not reset itself
- ValueTaskSource : Releated to `IValueTaskSource`
- Core : Shared structure

Provides the core logic for implementing an IValueTaskSource with a manual reset event.

# False sharing

Multiple workers tries to read the same cache line.
A cache line is an allocated low memory concept : For example, an array is allocated on 64B on a CPU, knowing that a CPU has its own level 1 cache.

To avoid this concurrent access, we could allocate the memory elsewhere by forcing it in the code.
Same as memory padding with C# accessing C++ code.

Instead of doing this :

```csharp
int[] array = new int[Size];
```

we can change it to :

```csharp
// IntsPerMemoryCacheLine: 16
int[] array = new int[Size * IntsPerMemoryCacheLine];

// Write in for loop
array[i * IntsPerMemoryCacheLine] = value;
```

This will pad the memory to have a faster access to the data.

This problem is a plateform agnostic problem, it happens on JVM or with C++ also.
