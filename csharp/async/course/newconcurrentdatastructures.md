---
title: New Concurrent Data Structures
description: https://academy.dotnetos.org/courses/take/async-expert-en/lessons/13606638-introduction
published: true
date: 2022-02-15T14:37:39.955Z
tags: 
editor: markdown
dateCreated: 2020-06-19T08:48:39.612Z
---

# Pipelines 

Originaly named Channels in some documentation.

Concept of consumer and producer.

To manage chunks of bytes, connected with IO (Like the http socket example).

# Channels

An asynchronous reader / writer.
There is unbounded (unlimited) and bounded (limited) channels.

An unbounded channel accepts unlimited amount of data.

[An Introduction to System.Threading.Channels, December 11th, 2019](https://devblogs.microsoft.com/dotnet/an-introduction-to-system-threading-channels/)

