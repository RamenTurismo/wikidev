---
title: Sync vs Async
description: 
published: 1
date: 2020-09-15T13:21:36.461Z
tags: 
editor: undefined
dateCreated: 2020-09-15T12:15:08.461Z
---

# Sync over async

Blocks the thread and wait for the operation to finish, problably making the threadpool starve.

Could lead to deadlocks.

> It needs to be avoided as much as possible.
{.is-danger}

## Examples

### Scenario 1

```csharp
public int GetInt()
{
  return GetIntAsync().Result;
}
```

This operation **blocks the thread**, and **could lead to deadlock**.
If there is an exception will be captured by the Task, and re-throw it in a `AggregateException`.

### Scenario 2

```csharp
public int GetInt()
{
  return GetIntAsync().GetAwaiter().GetResult();
}
```

Still blocks the thread, and still could lead to deadlock.

The only advantage is it throws the original exception instead of `AggregateException` compared to `.Result` and `.Wait`.
The decision was made to throws only one exception with `.GetAwaiter().GetResult()` (because await / async is only one operation).

### Scenario 3

```csharp
public int GetInt()
{
  return Task.Run(() => GetIntAsync()).Result;
}
```

This gets ride of deadlocking, because it does not capture the `SynchronizationContext`, thus the scheduling will be done on the `ThreadPool`.

This has overhead: We are blocking the main thread, and also taking another thread from the `ThreadPool`. Could lead to ThreadPool starvation if overused.

### Scenario 4

```csharp
public int GetInt()
{
  return Task.Run(() => GetIntAsync()).GetAwaiter().GetResult();
}
```

Same as **Scenario 3**, but with the same advantages as **Scenario 2**.

# Blocking detector

`Ben.BlockingDetector`'s nuget package detectes blocking operations at runtime, and shows warnings.

# Constructors

> `.Result` method should not be used.
{.is-danger}

## Wrong examples

```csharp
public class Service : IService
{
  private readonly Task<int> _data;
  public int Data => _data.Result;
  
  public Service(IAsyncDependency asyncDependency)
  {
    _data = asyncDependency.DoAsync();
  }
}
```

It kicks off `DoAsync`, leaving the Task in an unmanaged state, non-observed at the time and without any error handling.

## Solutions

The factory pattern solution:

```csharp
public class Service : IService
{
  private readonly int _data;
  
  public Service(int data)
  {
    _data = data;
  }
  
  public static async Task<Service> CreateAsync(IAsyncDependency dependency)
  {
    int data = await dependency.GetAsync();
    return new Service(data);
  }
}
```

Asynchronous Initialization Pattern from [Async OOP 2: Constructors by Stephen Cleary](https://blog.stephencleary.com/2013/01/async-oop-2-constructors.html):

```csharp
public interface IAsyncInitialization
{
  Task Initialization { get; }
}

public class Service : IService, IAsyncInitialization
{
  pubilc int Data { get; private set; }
  public Task Initialization { get; }
  
  private async Task InitializeAsync(IAsyncDependency dependency)
  {
    Data = await GetAsync();
  }
  
  public Service(IAsyncDependency dependency)
  {
    Initialization = InitializAsync(dependency);
  }
}

// Initialization of the service is a bit more "complex"
var service = new Service(dependency);
await service.Initialization;
```

> Renders things complex with **IoC containers**. We need to implement ourselves the service initialization and remember to call the methods.
Also, async await may not be easy to implement at the configuration level of the container.
{.is-warning}

```csharp
var service = provider.GetService<Service>();
if(service is IAsyncInitialization asyncService)
{
  await asyncService.Initialization;
}
```

# Async over sync

Not dangerous (compare to sync over async).
Best way to do that is to go async all the way. Look at existing APIs if they contain async methods.

`Task.Run` would be used to delegate the synchronous operation.

[See example here](https://gitlab.com/RamenTurismo/async-expert-course/-/tree/master/SyncVsAsync).

> An adapted method from sync to async should **never** return **null**. **null** shouldn't be used as an error propagator because all the current APIs always expect a `Task` to be returned.
> Instead use, `Task.FromException`.
{.is-warning}
