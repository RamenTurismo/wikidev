---
title: Task Completion Source
description: 
published: 1
date: 2020-10-19T11:30:53.132Z
tags: 
editor: undefined
dateCreated: 2020-09-28T11:22:19.553Z
---

# Source

* [Danger of TaskCompletionSource](https://devblogs.microsoft.com/premier-developer/the-danger-of-taskcompletionsourcet-class/)

# Why

Used to implement the "TAP" pattern, from old legacy async code that does not implements it.

# Usage

> Be aware that `.SetResult` is executed **synchrously** (On the same thread where `TaskCompletionSource` was created).
{.is-warning}

`.SetResult` inlines the continuation of the exposed `Task`

It's considered good practice to always include `RunContinuationAsynchronously` in `TaskCompletionSource`'s constructor.

```csharp
public Task<int> GetSomethingAsync()
{
  var tcs = new TaskCompletionSource<int>(TaskCreationOptions.RunContinuationAsynchronously);
  
  var operation = new LegacyOperation();
  operation.Completed += result =>
  {
    // Code will resume on a different thread.
    tcs.SetResult(result);
  };
  
  return tcs.Task;
}
```