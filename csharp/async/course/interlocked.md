---
title: interlocked
description: 
published: 1
date: 2022-01-21T09:04:03.582Z
tags: 
editor: markdown
dateCreated: 2022-01-21T09:04:03.582Z
---

# Interlocked

A utility class to do atomic operations, such as changing a field's value from multiple threads.

It is compiler intrinsic.

## Increment, Decrement

`Interlocked.Increment` will increment a value.

## Add

Adds a value to the memory.

## Read(long)

Permits to read the entire a long (8 bytes) value in case the memory doesn't allow it to.

## Exchange

Returns the previous value before the swap.
Assigns the new value to the oldValue.

## CompareExchange

Changes the value only if the excepted value is met.
