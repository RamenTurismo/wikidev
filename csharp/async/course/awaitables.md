---
title: Awaitables
description: What is an await after all ?
published: 1
date: 2021-02-10T13:56:54.599Z
tags: 
editor: undefined
dateCreated: 2021-01-22T13:43:12.650Z
---

# Sources

* [Repository for examples](https://gitlab.com/RamenTurismo/async-expert-course/-/tree/master/DemoApp/5%20-%20Awaitables)
* [await anything](https://devblogs.microsoft.com/pfxteam/await-anything/)

# Awaiter behavior

`await` uses the interface `INotifyCompletion` to call a method when `IsCompleted` must be set to `true`.

Taking this sample :
```csharp
internal class SimpleAwaiter : INotifyCompletion
{
    public bool IsCompleted => true;

    /// <inheritdoc />
    public void OnCompleted(Action continuation)
    {
        // Never called.
        Console.WriteLine("Completed!");
    }

    public string GetResult() => "done!";
}
```

`GetResult` is already called because `IsCompleted` is always returning `true`.

## Continuation

`OnCompleted` has an action as a parameter, corresponding to the continuation method. The continuation method is the method that comes after the await.

# Await anything

Example on how to await a `Timespan` object to wait what the timespan describes:

```csharp
public static async Task StartAsync()
{
    Console.WriteLine("Awaiting timespan from 3 seconds");
    await TimeSpan.FromSeconds(3);
    Console.WriteLine("Done !");
}

public static TaskAwaiter GetAwaiter(this TimeSpan timeSpan)
{
    return Task.Delay(timeSpan).GetAwaiter();
}
```

The keyword `await` allows the compiler to search for the `GetAwaiter` method and takes the extension method created.

# Yielding

As `Task.Yield` does ([And seen in earlier notes](/csharp/async/course/tasks#taskyield)), it tells the continuation to launch the code after the `await` to the current synchronization context at the moment of the `await` or queue the work item on the thread pool.

It uses the `struct`, `YieldAwaiter`.

> `Task.Yield()` is not the same as `Thread.Yield()`.
{.is-warning}

## UI apps

The usage of `Task.Yield` in an UI app does not make the UI responsive, because the work items posted on the `SynchronizationContext` of the UI **is prioritized** compared to the input and rendering work.

To avoid using `Task.Yield` in an UI app, use the decicated APIs included in the framework.

For example, `Dispatcher.Yield` in WPF, allowing to get back to the context of the UI.