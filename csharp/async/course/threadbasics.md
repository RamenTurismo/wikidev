---
title: Thread basics
description: Explanation about threading, from CPU to Code
published: true
date: 2021-06-02T10:25:33.406Z
tags: async
editor: markdown
dateCreated: 2020-07-30T15:13:22.795Z
---

# Sources

- [Priority-induced starvation: Why Sleep(1) is better than Sleep(0) and the Windows balance set manager](http://joeduffyblog.com/2006/08/22/priorityinduced-starvation-why-sleep1-is-better-than-sleep0-and-the-windows-balance-set-manager/)
- [Microsoft Docs - Asynchronous programming](https://docs.microsoft.com/en-us/dotnet/csharp/async)

# Hardware thread

Holds the information to run **software threads**.

# Software thread

A software thread contains its own context of execution.
It represents a single unit of execution on the OS level, scheduled to execute the code on the CPU.

Creating too many threads could be performance taking, as it contains many metadata.

Only a limited amount of software thread can be executed on the hardware thread.

# Context switching

**Context switching** is the fact of switching the current executed thread to another thread waiting to be executed.

A context contains many metadata to restore such as the last executed code pointer (IP), current mode, priority, state, etc.

# Types of codes 

**Methods CPU Bound**
Code is making calculations (Dividing, etc)

**Methods I/O Bound**
Code that is accessing memory

# Scheduling

## Preemptive

Code execution in a timely manner : threads are interrupted at a fixed rate to execute code.

## Quantum-based

Windows lets configure this.

![processor_scheduling_configuration.png](/processor_scheduling_configuration.png)

Programs: User default
Background services: Server default

## Priority-based

Has idle, lowest, below normal, normal, above normal, highest, time critical.
Number from 0 to 31, where à is the "zero paged thread".

Those priorities can be set inside a realtime application.
Setting too many threads priority too high can result in other programs having thread starvation, and can go worse by blocking the others applications.

# Thread scheduler

The scheduler can change the priority of the thread, change the owned lock thread, for the I/O completion called priority boost. Allows to not starve a low priority thread (Starving a thread is also called a **Race condition**).

The thread scheduler bases his actions on the thread state Running, Waiting, Ready, StandBy, Terminated, Initialized, etc.

For example, the thread scheduler will run Thread1 (Called **T1**) for a whole quantum time, then at the next quantum time, it changes to a `Waiting` state.
The scheduler will arange **T1** to be kicked of the hardware thread, and be replaced by a lower priority thread that is in the `Ready` state to keep things smooth.
At the end of that quantum time, if **T1** is ready, it will replace the current running thread by **T1** and so forth.

## Thread contention, starvation, CPU Trashing

Also called thread starvation, it is the fact of having too many running software threads. 

This can cause a lot of context switch, because they could possibly all waiting and go to the ready state all at the same time (refered also as **CPU trashing**), which will be a performance taker.

# Hyperthreading

This is the fact of taking advantage of the code that pauses to access i.e. memory and executing another piece of code during these pauses so that there is no pauses.
![hyperthreading](/csharp/hyperthreading.png)
Spliting one hardware core into two logical cores.

**Conclusion**
A limited number of software threads can be executed on the hardware thread.

# Threads in .NET

- Native thread
- Unmanaged thread
  - Backed by native thread
- Managed thread
- Executes code
- Foreground thread
  - When closing: App waits for these to end
  - Thread pool is based on foreground threads
- Background thread
  - When closing: App **doesn't** wait for these to end
  - Thread with more information in the context
  
Every **worker thread** contains a queue of work items.
  
## Thread API  
### Exception handling
  
If an exception is thrown from a thread executed code, it will kill the application.

Can be observed via `AppDomain.CurrentDomain.UnhandledException`.

### Thread.Sleep(int)

Puts the current thread in the `Waiting` state for a given amount of time, and put it back to the `Ready` state via a callback method.

It uses the system clock (system timer resolution), which can be checked via `ClockRes` tool.
The timer resolution can be changed by programs, default is `15.6 ms`.

**Google Chrome** changes it to **1 ms** for responsive purposes.

### Thread sleeping

In code loops, some time may be needed to allow a Thread to launch some code.

Multiple methods are used
- Thread.Sleep(1)
  - Slowest
  - Forces context switch
- Thread.Sleep(0)
  - May lead to busy waiting (Many threads a waiting another thread to be ready)
  - Thread chancing condition may have lower priorty, a risk of starvation and inefficiency
- Thread.Yield()
  - Fast operation
  - Give back time slice to a `Ready` thread
- Thread.SpinWait(int iterations)
  - Calls a special CPU instruction *n* number of times, so it can give pauses to call threads
  - Intepretation of the call changes depending on the CPU
    - i.e. Between pre-Skylake processors (14-17 yields) and post-Skylakes processors (125-150 yields)
    
### Thread termination

`Thread.Abort` or `Thread.Interrupt` should not be called because they throw out of bound exceptions that are not possible to handle.
These APIs are not supported in **.NETCore**.

### Thread cancellation

With the cancellation concept. The usage `CancellationTokenSource` and `CancellationToken`.
`CancellationToken` can be passed to every method to cancel the on going operation.

`CancellationToken` can have a callback method via the `.Register` method. This methods returns `CancellationTokenRegistration` which needs to be passed to `.Unregister`, and disposed when not used anymore.

# Thread pool

A concept to re-use threads.

Manages 2 types of threads :

- worker threads (execute work items, timer expiration callbacks, registration callbacks)
- I/O completion port thread (IOCP) (only I/O callbacks) which are **background threads**.

> The term 'I/O thread' in .net/CLR refers to the threads the ThreadPool reserves in order to dispatch NativeOverlapped callbacks from "overlapped" win32 calls (also known as "completion port I/O"). The CLR maintains its own I/O completion port, and can bind any handle to it (via the ThreadPool.BindHandle API).  

It contains only **background threads**.

There is only **one** thread pool **per application**, or per dotnet runtime. Cannot be created multiple times. 

> Example : AspNetCore, WCF, timers, uses the ThreadPool.
{.is-info}

Can be re-implemented by an application if necessary, for example, to implement a priority system for work items.

Threads are destroyed after some time, also called *Trimming*
It has inside balancing mechanics to determine whether there is too much or not enough threads waiting, running.

## .NET

`ThreadPool.QueueUserWorkItem(WaitCallBack);` queues a work item to be executed later on.

### Exception handling
Thrown exceptions are *unhandled*. Can use `AppDomain.CurrentDomain.UnhandledException` to observe.

### Termination

**Background threads** does not have any guarantee of terminating.
There is no simple way.

A technic is to implement a synchronization mechanism, such as marking work items to know when they're done.

In the callback method, you can switch the current thread to be a **Foreground thread**, to ensure that the work item is done.
It works because `IsBackground` is reset when the thread ends.

It has problems:
- The main thread may end before queued work item was started
- Even if enqueued, the work item could not be able to change `IsBackground` to `false` in time

## Managment

Starts with a default number of threads
Will avoid to create threads as much as possible (because of potential too much context switches).

Too few threads: Waste of CPU if too many work items are waiting
Too many threads: **context switching**, cache misses and **CPU trashing**.

### Maximum number of threads

Represents the maximum number of threads that the thread pool can manage.
Depends of the runtime used. 

> .NETCore x64 has a maximum of 32,768 worker threads and 1000 IOCP Threads.
{.is-info}

- *soft* thread pool starvation is the spike that can come when we are in the "normal zone" (above minimum), slowly creating threads. This can lead to performance issues (*i.e.* JIT cannot optimize because of the starvation).

### Minimum number of threads

Not a real minimum. Can actually be fewer threads in the thread pool.
Is actually a **threshold**.
If below minimum, create threads immediately as needed.
If above minimum, create every new thread with the interval 0.5sec (max)

> .NETCore x64 has a minimum of 8 worker threads and IOCP Threads.
{.is-info}

Minimum value is the number of logical cores.
Raising it may improve concurrency and better spike response in case of blocked threads.

### Behavior of maximum and minimum threads

![threadpool_minimum_threshold_diagram.png](/csharp/threadpool_minimum_threshold_diagram.png)

There is 2 main algorithms to manage threads in "normal zone".
- *Starvation-avoidance* is adding worker threads if it sees no progress being made on queued items.
- *Hill-climbing heuristic* is maximizing throughput while using as few threads as possible, by injecting and removing threads.

## Configuration

### NETCore

In .NETCore, the configuration can be put in `[appname].runtimeconfig.json` (Per app).

[Deep-dive into .NET Core primitives, part 3: runtimeconfig.json in depth](https://natemcmaster.com/blog/2019/01/09/netcore-primitives-3/)

```json
{
  "runtimeOptions": {
    "configProperties": {
      "System.GC.Server": true,
      "System.GC.Concurrent": true,
      "System.Threading.ThreadPool.MinThreads": 4,
      "System.Threading.ThreadPool.MaxThreads": 8
    },
    ...
  }
}
```

### Programmatically

```csharp
ThreadPool.SetMinThreads(int workerThreads, int completionPortThreads);
ThreadPool.SetMaxThreads(int workerThreads, int completionPortThreads);
```

Cannot set the maximum number of worker threads or I/O completion threads to a number smaller than the number of logical cores on the computer.

## Waits

Called **Registered wait**, allows to asynchronously wait with a wait handle, instead of blocking the synchronous `.WaitOne()` that blocks the thread.

Example in repository, `ThreadExperiment` → `ThreadPoolWaitTest.cs`.

## Queuing

ThreadPool has 2 types of queues :

- Global task queue
  - If the item has been enqueued from a thread that does not belong to a thread pool.
  - `ThreadPool.QueueUserWorkItem`, `ThreadPool.UnsafeQueueUserWorkItem` is used.
  - Calling `Task.Yield` on the default scheduler

- Local task queues for each worker thread
  - When the thread comes from a thread pool.

## Dequeuing

Takes the LIFO order (Last In First Out) to preserve cache and data locality. Work items will be executed with more probable data.

If empty, into global queue with FIFO order (First In First Out).
If empty, into local queues of other threads with FIFO order. It helps the other threads for balancing.

[.NET Threadpool starvation, and how queuing makes it worse](https://labs.criteo.com/2018/10/net-threadpool-starvation-and-how-queuing-makes-it-worse)

- Constant server spikes
- ThreadPool is starved *softly*

It will enqueue work items into the global queue because the traffic spike. Every items coming into the global queue also creates an item into the local queue (because of the spike), enqueues it and **waits** for it.

New threads will be created (because of starvation). It will look into the local (empty because of just created) and global queue, which is growing.

Enqueuing to the global queue is faster that the threadpool grow, which is a loop that will never close.
Task in the local queue are not processed, because of the new created threads in the global queue. Worker threads stands with the `Waiting` state.

There is a solution: instead of enqueuing to the **global queue**, we can enqueue to the **local queue**.
When threads are created because of starvation, it will look into the local queue (empty because just created), global queue (empty, because nothing is enqueued) and it will try to balance by stealing work from other worker threads.

Never wait **synchronously** inside the thread pool, it will waste resources or even deadlock the app.

## I/O Operations

Make it async called **overlapped I/O**.

I/O Completion Port (IOCP): To obverse the result on **I/O Threads**
Represents many operations, most of the apps only have 1 port

The continuation callback is queued to the associated `SynchronizationContext`.
Meaning the callback could continue on a different thread from the initial one if needed.

## Monitoring

[dotnet counters](https://docs.microsoft.com/en-us/dotnet/core/diagnostics/dotnet-counters) can show the current ThreadPool state.

The following cases can occur with their conclusions :

- `Completed Work Item Count / 1 sec is slowing` = starvation, deadlock
- `Queue Length` is growing = starvation
- `Thread Count` is growing = starvation

`dotnet-trace` tells us what's happening about the running application. Has details about running threads.

By dump:
![memorydump_threadpool.png](/csharp/memorydump_threadpool.png)

## NETCore 2.1+

Kestrel uses managed sockets.

Configurable via JSON or code.

Json example (appsettings.json):
```json
{
	"Kestrel": {
  	"Limits": {
    	"MaxConcurrentConnections": 100,
      "MaxConcurrentUpgradedConnections": 100
    }
  }
}
```

`SocketTransportOptions.IOQueueCount` is configurable in `ConfigureWebHostDefaults`, `builder.UseSockets(options => options.IOQueueCount = 0)`.
0 means directly scheduled to the thread pool. Best configuration is to have as much Queues as the logical cores count.

For linux:
`RedHat.AspNetCore.Server.Kestrel.Transport.Linux` uses async I/O.


## Asynchronous Programming Model (APM) (Obsolete)

Methods with `Begin...` and `End...` 

```csharp
public IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state);

public int EndRead(IAsyncResult asyncResult);
```

Consists of CallBacks

Possibility to wrap those operations for modern C#
`Task<>.Factory.FromAsync(...`

## Event Asynchronous Pattern (EAP) (Obsolete)

Asynchronous operations calls C# event handlers for results

Was used for UI Apps. Needed for implicit synchronization with proper context (UI)

Supported: `WebClient`, `BackgroundWorker`

Methods are suffixed with `Async` but **were not designed** for `async await` (Misleading name).

## Task based Asynchronous Pattern (TAP)

See [Tasks](../course/tasks)
