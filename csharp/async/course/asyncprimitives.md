---
title: Async Primitives
description: 
published: 1
date: 2022-01-21T09:04:03.582Z
tags: 
editor: markdown
dateCreated: 2022-01-21T09:04:03.582Z
---

# Async lock

We could use `SemaphoreSlim` :

```csharp
static SemaphoreSlim semaphore = new SemaphoreSlim(1,1);

public async Task<string> Get(int id) 
{
  await semaphore.WaitAsync();

  try
  {
    return await MyMethodAsync(id);
  }
  finally
  {
    semaphore.Release();
  }
}
```

Or AsyncEx library contains the AsyncLock type.
