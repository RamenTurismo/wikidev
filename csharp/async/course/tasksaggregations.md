---
title: Tasks aggregations
description: 
published: true
date: 2021-11-29T16:21:12.723Z
tags: async
editor: markdown
dateCreated: 2021-11-29T15:06:17.921Z
---

# Task.WhenAll

Combines `Task`s into a single promise.

In the case of any Task throws, an `AggregateException` will be thrown.

## Example

```csharp
Task<int> t1 = Task.FromResult(0);
Task<int> t2 = Task.FromResult(1);
int[] results = await Task.WhenAll(t1, t2);
```

## vs Parallel

Is not the same thing : the Parallel library has an `Action<T>` callback.
> This will create an `async void` method.
{.is-warning}

> In .NET 6, `.ForEachAsync<T>` is available.
{.is-info}

## vs foreach

`await`ing in the loop should be the default choice for better exception handling.
Work is done in a sequential manner.


# Task.WhenAny

Continue the process when any of the `Task`s are completed. 

It does not throw, because it returns the `Task` that has completed.

> .NET 5 has introduced a new overload to avoid the overhead of array allocation with the overload `params Task[]`
{.is-info}
