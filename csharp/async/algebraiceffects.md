---
title: Asynchronous algebraic effects
description: 
published: 1
date: 2020-07-27T15:01:59.834Z
tags: 
editor: undefined
dateCreated: 2020-07-27T14:46:53.496Z
---

# Source

* [Effect Programming in C#](https://eiriktsarpalis.wordpress.com/2020/07/20/effect-programming-in-csharp/)

# Concept

> **Personal note** Looks like EAP which is legacy and deprecated.
{.is-info}


At its core, the library defines a task-like type, Eff, which can be built using async methods:

```csharp
using Nessos.Effects;
 
async Eff<int> Sqr(int x) => x * x;
```

and can be composed using the await keyword:

```csharp
async Eff<int> SumOfSquares(params int[] inputs)
{
    int sum = 0;
    foreach (int input in inputs)
    {
        sum += await Sqr(input);
    }
    return sum;
}
```
  
An Eff computation has to be run explicitly by passing an effect handler:

```csharp
using Nessos.Effects.Handlers;
 
Eff<int> computation = SumOfSquares(1,2,3);

computation.Run(new DefaultEffectHandler());
```
 
It should be noted that unlike Task, Eff values have delayed semantics and so running

```csharp
async Eff HelloWorld() => Console.WriteLine("Hello, World!");
Eff hello = HelloWord();
```

will have no observable side-effect. It is useful to think of Eff values as delegates, as opposed to futures.

Finally, Eff methods are capable of awaiting regular async awaitables

```csharp
async Eff Delay()
{
    await Task.Delay(1000);
}
```
  
It should be noted however that the opposite is not possible, that is Eff values cannot be awaited inside regular async methods.