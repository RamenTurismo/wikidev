---
title: Architecture with .NET
description: 
published: true
date: 2022-07-11T10:15:11.580Z
tags: 
editor: markdown
dateCreated: 2022-07-11T10:13:42.248Z
---

# Sources

* [REST APIs for Microservices? Beware!](https://codeopinion.com/rest-apis-for-microservices-beware/)
* [Blocking or Non-Blocking API calls?](https://codeopinion.com/blocking-or-non-blocking-api-calls/)