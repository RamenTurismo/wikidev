---
title: Logging
description: Ways of logging in a .NET environment
published: 1
date: 2021-01-29T16:09:35.081Z
tags: 
editor: undefined
dateCreated: 2021-01-29T16:05:58.751Z
---

# Microsoft.Extensions.Logging

By far the most recommended one.

It allows to integrate logging in the dependency container (`Microsoft.Extensions.DependencyInjection`) which is widely used in ASP.NETCore applications.

Each provider corresponds to a package. e.g. `Debug` → `Microsoft.Extensions.Logging.Debug`.

## Filters

Filters are done with the class names.

Here is a basic class : 
```csharp
namespace MyProgram.Services.Integrate
{
  public class MyClass
  {
    private readonly ILogger _logger;
    
    public MyClass(ILogger<MyClass> logger)
    {
      _logger = logger;
    }
  }
}
```

The filter for this class would be `MyProgram.Services.Integrate.MyClass`.

In the appsettings, you would have the following below to limit this class to the `Information` logging and more.

```json
{
  "Logging" :
  {
    "LogLevel" :
    {
      "Default" : "Error",
      "MyProgram.Services.Integrate" : "Warning",
      "MyProgram.Services.Integrate.MyClass" : "Information"
    }
  }
}
```

Here, `"MyProgram.Services.Integrate" : "Warning"` limits every class from this namespace will output logs with the `Warning` level and more.

## Providers & Defaults

Some providers have defaults (Like [AppInsight's](https://docs.microsoft.com/en-us/azure/azure-monitor/app/asp-net-core#how-do-i-customize-ilogger-logs-collection)).

This can be changed by specifiying the provider in the configuration. The following example will set AppInsight output default to `Information` and Debug output default to `Trace`.

```json
{
  "Logging" : {
    "LogLevel" : {
      "Default" : "Error",
      "MyProgram.Services.Integrate" : "Warning",
      "MyProgram.Services.Integrate.MyClass" : "Information"
    },
    "AppInsights" : {
      "Default" : "Information" 
    },
    "Debug" : {
      "Default" : "Trace"  
    }
  }
}
```