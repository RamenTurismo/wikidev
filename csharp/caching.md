---
title: Caching
description: 
published: 1
date: 2020-02-28T09:16:52.870Z
tags: 
editor: undefined
dateCreated: 2020-02-28T09:16:51.425Z
---

# With Redis

https://www.red-gate.com/simple-talk/dotnet/net-development/overview-of-azure-cache-for-redis/

# InMemory

Use [Microsoft.Extensions.Caching.Memory](https://www.nuget.org/packages/Microsoft.Extensions.Caching.Memory/).