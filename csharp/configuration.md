---
title: Configuration
description: How to create a configuration for your project
published: true
date: 2022-01-13T08:57:01.132Z
tags: 
editor: markdown
dateCreated: 2022-01-13T08:57:01.132Z
---

# Microsoft.Extensions.Configuration

## IConfiguration

This is an interface that contains all of our configuration, regardless from where it comes from.

Here's an example from a JSON file :
```json
{
  "IsEnabled": true
  "Clients" : {
    "MyClient" : {
 			"Url": "https://hellowor.ld/api"
		}
  }
}
```

To access a value, we could do so :
```csharp
bool isEnabled = configuration["IsEnabled"];
```
or like this with the `Microsoft.Extensions.Configuration.Binder` package
```csharp
public record class ClientOptions
{
  public Uri Url { get; init; }
}

ClientOptions options = new ClientOptions();
configuration.Bind("Clients:MyClient", options);

// Or like this
Uri url = configuration.GetValue<Uri>("Clients:MyClient:Url");
```

## Read from appsettings.json

`appsettings.json` is the default file that is read for an ASP.NET Core project.
This file can also be used with any other type of project.

The package `Microsoft.Extensions.Configuration.Json` provides the means to read from a json file via `ConfigurationBuilder`.

### Example

```csharp
IConfiguration configuration = new ConfigurationBuilder()
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", false)
    .Build();
```

## InMemory

In production, this should never be done. 
In a test environment, we could build our own `IConfiguration`.