---
title: C# Basics
description: Everything a new dev should know.
published: true
date: 2021-11-17T22:29:53.404Z
tags: 
editor: markdown
dateCreated: 2020-02-27T16:02:02.377Z
---

# class, struct and records

- A `class` is an object which point to a reference that then points to a pointer.
- A `struct` is an object passed by value. 
  It will be copied as many times as it needs to be read.
  It can be made read only with the keyword `readonly`.
  This is for anyone that uses `struct` a bit too much:
  > Arguably, structs can end up being more costly, because assignment copies a bigger value. So if   they are assigned a lot more than they are created, then structs would be a bad choice.
- A `record` is an addition on top of class or struct to make it immutable that implements an equality contract by default.
  It can be copied with the keyword `with`, and it'll become a new reference.
  It can be readonly when it is used with a struct.
  If the same `record` is copied, doing `myRecord == myCopy` will return `true`

# Ref Structs and structs

* [ref structs in C# 7.2 - .NET Concept of the Week - Episode 16](https://kalapos.net/Blog/ShowPost/DotNetConceptOfTheWeek16-RefStruct)

`struct` can live both on the **stack** and on the **heap**.

`ref struct` is a struct that can **only** live on the stack.

Here's an example of declaration :
```csharp
public ref struct MyStruct {
}
```

# Memory managment

C# uses the GC (Garbage Collector) to remove the unused memory.

It uses the **Stack** and the **Heap**.

# Comparing objects

## ==

This method usage is technical based.
The most common way to compare anything.

## .Equals(object)

This method usage is semantic based.
The `.Equals` method tests for **reference equality**.

# Get, Set and Init

Typically, a property can be created in a class like so :

```csharp
public class MyClass
{
  public int MyProperty { get; set; }
}
```

`{ get; set; }` indicates that the property can be read and written by anyone that can access it.

To make it only writtable by the class itself, we could use `{ get; private set; }`.

If the value is only written from the constructor, we could use `{ get; }`.

If the value is set via syntax sugar we could use `{ get; init; }` which would look like this :
`init` is not for only to be used with `record`.

```csharp
MyClass myObject = new()
{
  MyProperty = 1
};

// Compile time exception will occur !
myObject.MyProperty = 2;
```

# Arrays

To get an array range:

```csharp
var numbers = new { 1, 2, 3, 4, 5 };
var range = numbers[2..4];

// range: 3, 4
```

To get the last item:

```csharp
var numbers = new { 1, 2, 3, 4, 5 };
var range = numbers[^5..]
var lastItem = numbers[^1];

// lastItem: 5
// range: 5, 4, 3, 2, 1
```

## Sources

* https://www.codeguru.com/csharp/csharp/cs_misc/c-8.0-ranges-and-indices-types.html

# Bitwise enums

Making an enum bitwise allows us to use the bitwise `OR`.

Taking the following enum:

```csharp
public enum Position
{
  Front = 0b0001,
  Back = 0b0010,
  Left = 0b0100,
  Right = 0b1000
}
```

We can add a property as `FrontLeft`:

```csharp
FrontLeft = Front | Left,
```

# String interpollation

Interpollated strings are the fact of using `$` before declaring a string.
Basically, an interpollated string will call `string.Format` everytime it has to be translated.

To concatenate a string in C#, we can do it this way:
```csharp
int myVar = 123;
string example = "test" + myVar.ToString();
```

If used in a loop, it can have performance issues, as the string is recreated in memory everytime we loop into it, giving more to do to the garbage collector.
So, to use string interpollation, simply use `$` and the curly braces, to render the same result as above:

```csharp
int myVar = 123;
string example = $"test{myVar}";
```

We can even use classic `string.Format` style:

```csharp
double myVar = 123.45d;
string example = $"test{myVar:N1}";
string example2 = $"test{myVar,10:N1}";

// Culture: en-us
// Render: 
// test123.4
//           test123.4

```

Some performance improvements have been made with [C# 10 : Improved Interpolated Strings](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/proposals/csharp-10.0/improved-interpolated-strings)

# Events and strong references
If not managed properly, events can cause memory leaks.

The following causes a strong reference to `MyClass`:
```csharp
public MyClass
{
	public MyClass(EventClass eventClass)
	{
		eventClass.Event += My_Event;
	}
}
```

The event must be unsubscribed in any way possible, for example, with `IDisposable`.
```csharp
public MyClass : IDisposable
{
	private readonly EventClass _eventClass;

	public MyClass(EventClass eventClass)
	{
		_eventClass = eventClass;
	
		_eventClass.Event += My_Event;
	}
	
	public void Dispose()
	{
		_eventClass.Event -= My_Event;
	}
}
```

Using `IDisposable` is, in my opinion, a code smell, but the example above never happened to me so far.
# The different collections
## Hashset{T}

Contains a set of unique objects, the ones which `equals()` or the `ICompare` returns true when compared are not added / ignored when added.

### Exemple

Say we have this:
```csharp
int[] @array = new [] { 1, 2, 3, 3 };
```

With `Hashset`:
```csharp
// Result = { 1, 2, 3 }
```

## Stack{T}

Often use to manage Navigation in apps, were `GoBack` and `GoNext` are used.

## Queue{T}

`Queue` uses First-In-First-Out (FIFO) behavior.

Has it names indicates, it is to process object by object, like a queue often does, right.

## ConcurrentDictionary

Can be useful when working on a multi-threaded environment.

Allows to add values to it via the `TryAdd` method.

## BitArray
Stores `bool` values with taking a size or an array as a parameter.

# Tuple, ValueTuple

The `Tuple` and `ValueTuple` objects has been incorporeted in the language:

```csharp
Tuple<int, int> myVar = new Tuple<int, int>(1, 1);

// Named syntax.
(int x, int y) myVar = ValueTuple.Create(1, 1);
```