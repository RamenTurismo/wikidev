---
title: Windows service & workers
description: 
published: 1
date: 2020-03-31T13:11:45.148Z
tags: 
editor: undefined
dateCreated: 2020-03-31T13:11:43.597Z
---

# What's a worker

A worker is basically an AzureFunction.

To take the whole concept down, we got a "box" that contains the app (like an UWP app also).
This container manages the lifetime of the worker, which is a console app.

## Source

* https://www.stevejgordon.co.uk/what-are-dotnet-worker-services