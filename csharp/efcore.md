---
title: Entity Framework Core
description: Little tips & tricks about EFCore
published: true
date: 2023-01-04T14:44:31.430Z
tags: 
editor: markdown
dateCreated: 2020-10-21T09:24:10.183Z
---

# Linq

## Left Join

A left join is done through `.SelectMany()` and `.DefaultIfEmpty()`

```csharp
List<MyEntity> result = await _dbContext.MyEntity
  // Left Join
  .SelectMany(myEntity => _dbContext.MyOtherTable.Where(e => e.MyEntityId == myEntity.Id).DefaultIfEmpty(),
    // This is the selector, could be anything
    (myEntity, myOtherTable) => myEntity)
  .ToListAsync();
```

will generate

```sql
SELECT [t0].[MyEntityId]
FROM [dbo].[MyEntity] [t0]
LEFT JOIN [dbo].[MyOtherTable] ON [t0].[MyEntityId] = [t1].[Id]
```

## Left Join Composite

```csharp
Table[] results = await _db.Table
  .GroupJoin(dbContext.MyLeftJoinedTable,
    t => new { t.Key, SecondKey = 2 },
    leftJoinTable => new { leftJoinTable.Key, leftJoinTable.SecondKey },
    (t, leftJoinTable) => new { Table = t, leftJoinTable })
  .SelectMany(
    join => join.leftJoinTable.DefaultIfEmpty(),
    (join, leftJoinTable) => new { join.Table, leftJoinTable })
  .ToArrayAsync();
```

will generate

```sql
SELECT [t0].[Key], [t0].[Column], [t0].[SecondaryKey], [t1].[Key], [t1].[Column], [t1].[SecondaryKey]
FROM [dbo].[Table] [t0]
LEFT JOIN [dbo].[MyLeftJoinedTable] [t1] ON [t1].[Key] = [t0].[Key] AND [t1].[SecondaryKey] = [t0].[SecondaryKey]
```

# Sqlite

## InMemory: Quick setup

In your context:

```csharp
private static SqliteConnection GetAndOpenConnection()
{
    if (_sqliteConnection == null)
    {
        _sqliteConnection = new SqliteConnection("Data Source=:memory:");
        _sqliteConnection.Open();
    }

    return _sqliteConnection;
}
        
private static SqliteConnection _sqliteConnection;

protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
{
    base.OnConfiguring(optionsBuilder);

    if (!optionsBuilder.IsConfigured)
    {
        optionsBuilder.UseSqlite(GetAndOpenConnection());
    }
}
```

> Don't use this in production.
{.is-danger}

# Entity configuration

## Declare an UNIQUE key

We can use `.HasAlternateKey()` to create a key or composite key.

> This will be used by the tracker to determine what's being currently tracked.
> Meaning, an entity **cannot be tracked more than 1 time** with the **same key**.
{.is-warning}

For the reason above, it is recommended to use `.HasIndex(..).IsUnique()`. 
This way, it can be part of a Foreign Key.