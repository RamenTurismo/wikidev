---
title: C#: Async
description: 
published: true
date: 2021-05-26T12:23:18.889Z
tags: 
editor: markdown
dateCreated: 2020-02-25T12:52:00.974Z
---

# Summary

* [Algebraic effects](/csharp/async/algebraiceffects)
* ["Async Expert" course](/csharp/async/course)

# Async lock

Async locks can be done via `Semaphore`.

## Sources
* https://blog.cdemi.io/async-waiting-inside-c-sharp-locks/

# Task vs ValueTask

A task is consummable as many times as we want. The ValueTask is not.
> The underlying object may have been recycled already and be in use by another operation. In contrast, a `Task` / `Task<TResult>` will never transition from a complete to incomplete state, so you can await it as many times as you need to, and will always get the same answer every time.
  
Therefor, calling it concurrently should not happen.

> The underlying object expects to work with only a single callback from a single consumer at a time, and attempting to await it concurrently could easily introduce race conditions and subtle program errors. It’s also just a more specific case of the above bad operation: “awaiting a `ValueTask` / `ValueTask<TResult>` multiple times.” In contrast, `Task` / `Task<TResult>` do support any number of concurrent awaits.
  
Everything is better explained in the article linked in the sources.

## Sources

- [Why, What and Whens](https://devblogs.microsoft.com/dotnet/understanding-the-whys-whats-and-whens-of-valuetask/)

# How to use Tasks

There is a [good guidance here](https://github.com/davidfowl/AspNetCoreDiagnosticScenarios/blob/master/AsyncGuidance.md), that helped me understand how to use tasks.

It talks about the usages of Task in general, how to manage long thread operations to not block the used thread and the managment of sync and async.

## Sources

[Thread pool starvation](https://docs.microsoft.com/en-gb/archive/blogs/vancem/diagnosing-net-core-threadpool-starvation-with-perfview-why-my-service-is-not-saturating-all-cores-or-seems-to-stall)
[Guidance](https://github.com/davidfowl/AspNetCoreDiagnosticScenarios/blob/master/AsyncGuidance.md)
[Task asynchronous programming as an IO surrogate](https://blog.ploeh.dk/2020/07/27/task-asynchronous-programming-as-an-io-surrogate/)

