---
title: Typescript
description: Everything about typescript
published: 1
date: 2021-02-19T10:50:53.074Z
tags: 
editor: markdown
dateCreated: 2021-02-19T10:37:45.441Z
---

# Summary

* [Async Await](promises/)
* [Debug](debug/)
* [Linters](linters/)

# Frameworks

* [Ionic](https://ionicframework.com/)
A framework to create native apps with a single code base, with a JS/TS framework of choice.