---
title: Databases
description: What's your storage ?
published: true
date: 2021-11-17T16:57:50.219Z
tags: 
editor: markdown
dateCreated: 2021-04-15T09:39:09.919Z
---

# SQL

* Microsoft SQL
* SQLite

# NoSQL

* Azure Cosmos DB
  * SQL API
  * MongoDB API
* MongoDB

# PostgreSQL

## Connect as posgres

```
sudo -u postgres psql postgres
```