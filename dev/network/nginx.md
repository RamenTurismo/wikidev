---
title: Nginx
description: 
published: true
date: 2021-11-16T14:34:21.589Z
tags: nginx, server
editor: markdown
dateCreated: 2021-11-16T14:33:55.972Z
---

# Redirect from a subdomain

Add a server block :

```nginx
server 
{
    
} 
```