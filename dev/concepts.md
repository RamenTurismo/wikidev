---
title: Programming concepts
description: 
published: true
date: 2021-11-17T22:10:56.311Z
tags: 
editor: markdown
dateCreated: 2020-07-24T11:39:55.664Z
---

# SOLID

Regroups multiple basics principles.

# DRY

"Don't repeat yourself", applies to any code that you're writting.

# KISS

"Keep it simple, stupid!" - Kind of self explanatory.

# Convention over configuration

Always prioritize the default behavior over complex configuration.

For example, if you need to add more parameters to a command to run something, it means everyone has to know that they need to add those parameters, rendering the process more complex.

# Persistence ignorance principle
https://deviq.com/persistence-ignorance/

>The principle of Persistence Ignorance (PI) holds that classes modeling the business domain in a software application should not be impacted by how they might be persisted. Thus, their design should reflect as closely as possible the ideal design needed to solve the business problem at hand, and should not be tainted by concerns related to how the objects’ state is saved and later retrieved.

# Infrastructure ignorance principle
https://ayende.com/blog/3137/infrastructure-ignorance

>The infrastructure should not sit at the heart of the application. It should be shoved to some misbegotten corner and only see the light of day if there is no other choice

# Data concurrency & APIs

[Versionning the data](https://www.carlrippon.com/handling-concurrency-in-an-asp-net-core-web-api-with-dapper/)

# Feature envy

[Feature envy](https://refactoring.guru/smells/feature-envy) as here is :

> A method accesses the data of another object more than its own data.

To avoid this, using a concept like DDD could really help avoid all the cons that this brings to the code.