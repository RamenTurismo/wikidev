---
title: Network
description: 
published: true
date: 2021-11-17T20:06:23.749Z
tags: 
editor: markdown
dateCreated: 2021-11-16T14:39:39.492Z
---

# Enable HTTPS on your server

Using Let's encrypt which is a free CA provider.

Let's encrypt uses [certbot](https://certbot.eff.org/)

# Configuring a SSH server

## Security

If the SSH server is exposed to the internet, some things are to do to avoid getting yoinked :

- Disable password authentication
- Enable SSH access via a SSH key only
- Enable two factor authentication (Via Google Authenticator for example)
- Change the default SSH port