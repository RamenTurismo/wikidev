---
title: Tools
description: Tools used during my career
published: true
date: 2022-11-15T12:17:16.806Z
tags: 
editor: markdown
dateCreated: 2020-04-01T10:42:25.130Z
---

# Docker

A seperated file system that uses the kernel of the machine it runs on.

* [Getting started](https://docs.microsoft.com/en-us/visualstudio/docker/tutorials/docker-tutorial)

## VS Code

An extension exists named *docker* to see and manage machines that are running.

# IDEs

## Visual Studio Code

[Documentation](https://code.visualstudio.com/docs)

# Git

## SourceTree

A git tool made by Atlasian (BitBucket creators).

Very pratical for basic usage.

## Git Extensions

A git tool.

Very pratical for basic usage. Also has a context menu integration, which comes very handy when working with multiple repositories.

# Apache JMeter

* [Getting started](https://jmeter.apache.org/usermanual/get-started.html)

Useful for APIs load testing.

# Dump .dll library functions

Find `link.exe`. It comes with Visual Studio, here's an example of a path :
```
C:\Program Files\Microsoft Visual Studio\2022\Enterprise\SDK\ScopeCppSDK\vc15\VC\bin
```

Then, run the following command
```
.\link.exe /dump /exports C:\Windows\System32\user32.dll
```

# SysInternals

A buch of tools made by [Microsoft](https://learn.microsoft.com/en-gb/sysinternals/).

Most popular ones are being [ProcessExplorer](https://learn.microsoft.com/en-gb/sysinternals/downloads/process-explorer) and [TCPView](https://learn.microsoft.com/en-gb/sysinternals/downloads/tcpview).
