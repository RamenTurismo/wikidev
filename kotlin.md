---
title: Kotlin for Android
description: Everything about it.
published: true
date: 2022-03-15T08:22:20.472Z
tags: 
editor: markdown
dateCreated: 2020-02-25T13:48:07.187Z
---

I'll talk a lot about kotlin for Android.

# Summary

* [Architecture](/kotlin/architecture)
* [DataBinding](/kotlin/databinding/)

# Courses

* [Android & Kotlin | Apps Next Gen et Architecture Components](https://www.udemy.com/course/android-kotlin-developpez-des-apps-next-gen/learn/lecture/11602174?start=0#overview)

# Frameworks

* [Timber](https://github.com/JakeWharton/timber)
Logging framework.
* Room
ORM and DAO layer creator, communicating with a Sqlite database.

# Samples

* [Firebase Quickstart](https://github.com/firebase/quickstart-android)

# Projects

List of my projects using kotlin:
- [Gran Manager](https://gitlab.com/RamenTurismo/gran-manager)